+++
author = "Forrest Gump"
title = "First Test Page"
date = "2021-08-15"
description = "Test"
tags = [
    "Selenium",
]
image = ""
+++
Test Page
<!--more-->

{{< rawhtml >}}
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
{{< /rawhtml >}}
### This is a test page for a Selenium project</h1>
Here is a dropdown:  

{{< dropdown "second-test" >}}

The dropdown item will lead you to the next page, which has a form.
