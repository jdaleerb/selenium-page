+++
author = "Forrest Gump"
title = "Educational Records Bureau"
date = "2021-08-05"
description = "Jeffsum"
tags = [
    "Jeff Goldblum",
]
image = "img/jeff.jpg"
+++

There are many lorem ipsum generators out there. I like [Jeffsum](http://jeffsum.com). Life finds a way. 

Yeah, but your scientists were so preoccupied with whether or not they could, they didn't stop to think if they should. Jaguar shark! So tell me - does it really exist? God creates dinosaurs. God destroys dinosaurs. God creates Man. Man destroys God. Man creates Dinosaurs.
<!--more-->
So you two dig up, dig up dinosaurs? Just my luck, no ice. Must go faster... go, go, go, go, go! Hey, take a look at the earthlings. Goodbye! What do they got in there? King Kong? This thing comes fully loaded. AM/FM radio, reclining bucket seats, and... power windows.

You're a very talented young man, with your own clever thoughts and ideas. Do you need a manager? You really think you can fly that thing? My dad once told me, laugh and the world laughs with you, Cry, and I'll give you something to cry about you little bastard!

Yeah, but your scientists were so preoccupied with whether or not they could, they didn't stop to think if they should. This thing comes fully loaded. AM/FM radio, reclining bucket seats, and... power windows. Yeah, but your scientists were so preoccupied with whether or not they could, they didn't stop to think if they should.

My dad once told me, laugh and the world laughs with you, Cry, and I'll give you something to cry about you little bastard! Yeah, but John, if The Pirates of the Caribbean breaks down, the pirates don’t eat the tourists. Life finds a way. Must go faster... go, go, go, go, go!
